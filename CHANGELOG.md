**25/05/2018**

**v0.3.0**

    schema adds rubygems reverse dependencies  


**v0.2.0**

    schema adds rubygems activity feeds (latest and updated)  


**v0.1.0**

    first version
