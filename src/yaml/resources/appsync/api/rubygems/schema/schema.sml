type Dependency {
	name: String
	requirements: String
}
	
type Dependencies {
	development: [Dependency]
	runtime: [Dependency]
}

type Gem {
  parent_uri: String
	name: String
	downloads: Int
	version: String
	version_downloads: Int
	authors: String
	info: String
	project_uri: String
	gem_uri: String
	homepage_uri: String
	wiki_uri: String
	documentation_uri: String
	mailing_list_uri: String
	source_code_uri: String
	bug_tracker_uri: String
	dependencies: Dependencies
  reverse_dependencies: [String]
	versions: Versions
}

type Versions {
  parent_uri: String
  all: [Version]
}

type Version {
	authors: String
	built_at: String
	created_at: String
	description: String
	downloads_count: Int
	number: String
	summary: String
	platform: String
	ruby_version: String
	prerelease: String
	licences: String
	requirements: String
	sha: String
}
	
type Repository {
  feeds: Feeds
  gem(name: String): Gem
}

type Feeds {
  parent_uri: String
  latest: [Gem]
  updated: [Gem]
}

type Api {
  repository: Repository
}

type Query {
  rubygems: Api
}

schema {
  query: Query
}
