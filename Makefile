.PHONY: help build deploy generate

STACK_NAME=graphql-schema-rubygems

AWS_DEFAULT_REGION ?= us-east-1

S3_BUCKET_NAME ?= repo.dev.mswinson.com
S3_BUCKET_PREFIX ?= packages/com.mswinson/lib/cf/$(STACK_NAME)

S3_OPTIONS=--s3-bucket $(S3_BUCKET_NAME) --s3-prefix $(S3_BUCKET_PREFIX)

help:
	@sam

generate:
	@mkdir -p ./src/generated
	@erb -r ./lib/ruby/yaml_resource.rb -T - ./src/yaml/root.yml.erb > ./src/generated/template.yml

build: ./src/generated/template.yml
	@mkdir -p ./target
	@sam package --template-file ./src/generated/template.yml --use-json --output-template-file target/$(STACK_NAME).json $(S3_OPTIONS)

./src/generated/template.yml: generate
	@sam validate --template ./src/generated/template.yml

./target/$(STACK_NAME).json: build

deploy: ./target/$(STACK_NAME).json
	@AWS_DEFAULT_REGION=$(AWS_DEFAULT_REGION) sam deploy --template-file ./target/$(STACK_NAME).json --stack-name $(STACK_NAME) $(S3_OPTIONS) --capabilities CAPABILITY_IAM
